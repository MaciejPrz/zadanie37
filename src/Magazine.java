import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Magazine {
Map<PRODUCT_CLASS, List<Product>> produktZKlasa = new HashMap<>();
 //   private Map<PRODUCT_CLASS, List<Product>> map = new HashMap<>();

    public Magazine() {
        produktZKlasa.put(PRODUCT_CLASS.HIGH, new LinkedList<>());
        produktZKlasa.put(PRODUCT_CLASS.LOW, new LinkedList<>());
        produktZKlasa.put(PRODUCT_CLASS.MID, new LinkedList<>());
    }

    public void dodajProdukt(Product produkt) {

        produktZKlasa.get(produktZKlasa.getClass()).add(produkt);
    }
    public boolean sprawdzCzyProduktJestZDanejPolki(String nazwa){

        boolean flaga = false;
        for(Map.Entry <PRODUCT_CLASS, List<Product>> polka : produktZKlasa.entrySet()){
            for(Product p :polka.getValue()){
                if (nazwa.equals(p.getName())){
                    return true;
                }
            }
        }
return false;
    }
}
