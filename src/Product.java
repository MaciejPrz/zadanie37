public class Product {

    private String name;
    private int cena;
    private PRODUCT_TYPE type;
    private PRODUCT_CLASS PClass;


    public Product(String name, int cena, PRODUCT_TYPE type, PRODUCT_CLASS PClass) {
        this.name = name;
        this.cena = cena;
        this.type = type;
        this.PClass = PClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setType(PRODUCT_TYPE type) {
        this.type = type;
    }

    public PRODUCT_CLASS getPClass() {
        return PClass;
    }

    public void setPClass(PRODUCT_CLASS PClass) {
        this.PClass = PClass;
    }

    public Product(){

    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", cena='" + cena + '\'' +
                ", type=" + type +
                ", PClass=" + PClass +
                '}';
    }
}
